/*Configuración del evento 'change'*/

const comboBox = () =>{
    const origen = document.getElementById('monedaOri').value;
    const nueva = document.getElementById('monedaNuev');
    let nuevoOp = "";
    
    /*En este caso se realizara una manera manual insertando ls opciones en el HTML*/
    switch(origen){
        case "1": 
                 nuevoOp = `<option value="0" disabled selected>Moneda Nueva</option>
                <option value="2">Pesos Mexicanos</option>
                <option value="3">Dólar Canadiense</option>
                <option value="4">Euro</option>`;
                break;

        case "2": 
                nuevoOp = `<option value="0" disbaled selected>Moneda Nueva</option>
                <option value="1">Dólar Estadounidense</option>
                <option value="3">Dólar Canadiense</option>
                <option value="4">Euros</option>`;
               break;

        case "3": 
                nuevoOp = `<option value="0" disabled selected>Moneda Nueva</option>
                <option value="1">Dólar Estadounidense</option>
                <option value="2">Pesos Mexicanos</option>
                <option value="4">Euros</option>`;
                break;

        case "4": 
                nuevoOp = `<option value="0" disabled selected>Moneda Nueva</option>
                <option value="1">Dólar Estadounidense</option>
                <option value="2">Pesos Mexicanos</option>
                <option value="3">Dólar Canadiense</option>`;
                break;

        default: 
                return alert("Introduzca un valor valido");
    }

    nueva.innerHTML = nuevoOp;
}

/*Funcion para el evento calcular*/
const calcular = () =>{
        //Obtener los valores
        const cantidad = parseFloat(document.getElementById('valor').value);
        const origen = document.getElementById('monedaOri');
        const nueva = document.getElementById('monedaNuev');
        let resultado = 0.0;

        //Validacion de la cantidad
        if(isNaN(cantidad) || cantidad < 0){
                document.getElementById('valor').value = "";
                return alert("La cantidad ingresada no es valida");
        }

        //Calculo de las conversiones
        if(origen.value == "1"){        //Dolar estadounidense
                switch(nueva.value){
                        case "2":
                                resultado = cantidad * 19.85;
                                break;
                        
                        case "3":
                                resultado = cantidad * 1.35;
                                break;
                        
                        case "4":
                                resultado = cantidad * 0.99;
                                break;
                        
                        default:
                                return alert("Introduzca una opcion valida");
                }

        }else if(origen.value == "2"){  //Pesos Mexicanos
                switch(nueva.value){
                        case "1":
                                resultado = cantidad / 19.85;
                                break;

                        case "3":
                                resultado = (cantidad /19.85) / 1.35;
                                break;
                        
                        case "4":
                                resultado = (cantidad / 19.85) / 0.99;
                                break;
                        
                        default:
                                return alert("Introduzca una opcion valida");
                }

        }else if(origen.value == "3"){  //Dolar Canadiense
                switch(nueva.value){
                        case "1":
                                resultado = cantidad * 1.35;
                                break;
                        
                        case "2":
                                resultado = (cantidad / 1.35) * 19.85;
                                break;
                        
                        case "4":
                                resultado = (cantidad / 1.35) * 0.99;
                                break;
                        
                        default:
                                return alert("Introduzca una opcion valida");
                }
        }
        else if(origen.value == "4"){   //Euro
                switch(nueva.value){
                        case "1":
                                resultado = cantidad / 0.99;
                                break;
                        
                        case "2":
                                resultado = (cantidad / 0.99) * 19.85;
                                break;
                        
                        case "3":
                                resultado = (cantidad / 0.99) * 1.35;
                                break;

                        default:
                                return alert("Introduzca una opcion valida");
                }
        }else{
                return alert("Escoja una opcion valida");
        }

        //Llenando los inputs
        document.getElementById('SubTotal').value = (resultado).toFixed(2);
        document.getElementById('TotComision').value = (resultado * 0.03).toFixed(2);
        document.getElementById('TotPagar').value = (resultado * 1.03).toFixed(2);
}

//Funcion para limpiar
const limpiar = () =>{
        document.getElementById('valor').value = "";
        document.getElementById('SubTotal').value = "";
        document.getElementById('TotComision').value = "";
        document.getElementById('TotPagar').value = "";
        document.getElementById('monedaOri').value = "0";
        document.getElementById('monedaNuev').value = "0";

}

//Boton de registrar
const registrar = () =>{
        const registro = {
                original: document.getElementById('monedaOri').value,
                nueva: document.getElementById('monedaNuev').value,
                cantidad: document.getElementById('valor').value,
                subTotal: document.getElementById('SubTotal').value,
                totComi: document.getElementById('TotComision').value,
                totPag: document.getElementById('TotPagar').value
        };

        const moneda = (id) =>{
                switch(id){
                        case "1": return "Dólar Estadounidense";

                        case "2": return "Pesos Mexicanos";

                        case "3": return "Dólar Canadiense";

                        case "4": return "Euro";
                }
        }

        let tabla = document.getElementById('parrafo');

        registro.original = moneda(registro.original);
        registro.nueva = moneda(registro.nueva);

        tabla.innerHTML += "$" + registro.cantidad + "   " + registro.original + "  a  " + registro.nueva + "   $" + registro.subTotal + "   $" + registro.totComi + "   $" + registro.totPag + "<br>";
        limpiar();

}

//Funcion para borra los registros
const borrar = () =>{
        limpiar();
        document.getElementById('parrafo').innerHTML = "";
}

/*Llamado de las funciones con los eventos de los botones*/
document.getElementById('monedaOri').addEventListener('change', comboBox);
document.getElementById('calcular').addEventListener('click', calcular);
document.getElementById('registro').addEventListener('click', registrar);
document.getElementById('borrar').addEventListener('click', borrar);